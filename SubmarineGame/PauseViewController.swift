import UIKit

class PauseViewController: UIViewController {

    @IBOutlet weak var blurView: UIVisualEffectView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    
    var score: Int?
    var name: String? = "You"
    var needsAnimation = false
    
    override func viewDidLoad() {
        guard let name = name, let score = score else { return }
        
        nameLabel.text = "\(name) scored:"
        scoreLabel.text = "\(score)m"
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        if needsAnimation {
            view.alpha = 0
            UIView.animate(withDuration: 0.3) {
                self.view.alpha = 1
            }
        }
    }

    @IBAction func recordsButtonPressed(_ sender: UIButton) {
        needsAnimation = false
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "RecordViewController") as? RecordViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func menuButtonPressed(_ sender: UIButton) {
        needsAnimation = false
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    @IBAction func retryButtonPressed(_ sender: UIButton) {
        needsAnimation = false
        UIView.animate(withDuration: 0.3) {
            self.view.alpha = 0
        } completion: { _ in
            self.navigationController?.popViewController(animated: false)
        }
    }
}
