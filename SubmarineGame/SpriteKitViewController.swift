import UIKit
import SpriteKit
import GameplayKit
import CoreMotion

class SpriteKitViewController: UIViewController {
    
    private enum OxygenAction {
        case restore, decrease
    }
    
    private enum ObstacleType {
        case nemoFish
        case patrickStar
        case tigerFish
        case yellowFish
        case boat
    }
    
    private struct Obstacle {
        let isEnemy: Bool
        let name: String
        let scale: CGFloat
        let speed: TimeInterval
        let timePerFrame: TimeInterval
        let type: ObstacleType
        
        static func nemoFish() -> Obstacle {
            return Obstacle(isEnemy: true,
                            name: "NemoFish",
                            scale: 0.6,
                            speed: 7,
                            timePerFrame: 0.1,
                            type: ObstacleType.nemoFish)
        }
        
        static func boat() -> Obstacle {
            return Obstacle(isEnemy: true,
                            name: "Boat1",
                            scale: 0.13,
                            speed: 7,
                            timePerFrame: 0.1,
                            type: ObstacleType.boat)
        }
        
        static func patrickStar() -> Obstacle {
            return Obstacle(isEnemy: true,
                            name: "PatrickStar",
                            scale: 0.2,
                            speed: 7,
                            timePerFrame: 0.15,
                            type: ObstacleType.patrickStar)
        }
        
        static func yellowFish() -> Obstacle {
            return Obstacle(isEnemy: true,
                            name: "YellowFish",
                            scale: 0.3,
                            speed: 7,
                            timePerFrame: 0.1,
                            type: ObstacleType.yellowFish)
        }
        
        static func tigerFish() -> Obstacle {
            return Obstacle(isEnemy: true,
                            name: "TigerFish",
                            scale: 0.55,
                            speed: 7,
                            timePerFrame: 0.2,
                            type: ObstacleType.tigerFish)
        }
        
    }
    
    @IBOutlet weak var skyView: UIView!
    @IBOutlet weak var scoreLabel: UILabel!
    private var upGestureView: UIView!
    private var downGestureView: UIView!
    
    private var gameView = SKView()
    private var scene: SKScene!
    private var submarineNode: SKSpriteNode!
    private var sandNode: SKSpriteNode!
    private var submarine: SKNode!
    private var oxygenBar: SKSpriteNode!
    private var skyNode: SKShapeNode!
    private var touch: UITouch?
    
    private var intersectionTimer: Timer!
    private var obstaclesTimer: Timer!
    private var axelTimer: Timer!
    private var scoreTimer: Timer!
    
    private var currentAction: OxygenAction = .decrease
    private var settings: Settings?
    private var animationHash = UUID().uuidString
    private var defaultAxel: Double?
    private var axel = 1.0
    private var score = 0 {
        didSet {
            scoreLabel.text = "\(score)m"
        }
    }
    
    private let obstacles = [
        Obstacle.nemoFish(),
        Obstacle.tigerFish(),
        Obstacle.patrickStar(),
        Obstacle.yellowFish(),
        Obstacle.boat()
    ]
    private let motionManager = CMMotionManager()
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        settings = UserDefaults.standard.value(Settings.self, forKey: settingsKey)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        resetGame()
    }
    
    
    
    @objc func move(_ recognizer: UIPanGestureRecognizer) {
        let translation = recognizer.translation(in: self.view)
        
        guard submarine.position.y - translation.y < self.view.frame.height else { return }
        
        submarine.position = CGPoint(x: submarine.position.x,
                                     y: submarine.position.y - translation.y)
        
        recognizer.setTranslation(.zero, in: self.view)
    }
    
    @objc func moveAxel() {
        motionManager.startAccelerometerUpdates(to: .main) { [weak self] data, error in
            guard let self = self, let data = data, error == nil else { return }
            if self.defaultAxel == nil {
                self.defaultAxel = data.acceleration.z
            } else {
                let currentY = self.submarine.position.y
                let action = SKAction.moveTo(y: currentY - CGFloat((data.acceleration.z - self.defaultAxel!) * 500), duration: 0.5)
                self.submarine.run(action)
            }
            print(data.acceleration.z)
        }
    }
    
    private func prepareGameScene() {
        view.addSubview(gameView)
        
        scene = SKScene(size: view.frame.size)
        scene.backgroundColor = .clear
        
        addSubmarine(to: scene)
        addBubblesEmitter(to: scene)
        addSand(to: scene)
        addSunEmitter(to: scene)
        addSky(to: scene)
        
        gameAxeleration()
        spawnObstacles()
        
        gameView.frame.size = scene.size
        gameView.backgroundColor = .clear
        gameView.presentScene(scene)
        
        
        intersectionTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true) { _ in
            self.intersection()
        }
        
        scoreTimer = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { _ in
            self.score += 1
        })
        
        
    }
    
    private func addSubmarine(to scene: SKScene) {
        guard let textureName = settings?.submarine.rawValue else { return }
        submarineNode = SKSpriteNode(imageNamed: textureName)
        submarineNode.zPosition = 1
        submarineNode.xScale = 0.45
        submarineNode.yScale = 0.45
        submarineNode.position = CGPoint(x: 0, y: 0)
        
        guard let trail = SKEmitterNode(fileNamed: "Trail") else { return }
        trail.position = CGPoint(x: submarineNode.frame.minX,
                                 y: submarineNode.frame.midY - 10)
        trail.targetNode = scene
        
        oxygenBar = SKSpriteNode(color: .green,
                                 size: CGSize(width: submarineNode.frame.width,
                                              height: 8))
        oxygenBar.position = CGPoint(x: submarineNode.position.x,
                                     y: submarineNode.frame.maxY + 8)
        oxygenBar.zPosition = 1
        oxygenBar.name = "oxygen"
        
        submarine = SKNode()
        submarine.position = CGPoint(x: submarineNode.size.width / 2 + 50,
                                     y: view.frame.height / 2)
        submarine.addChild(submarineNode)
        submarine.addChild(trail)
        submarine.addChild(oxygenBar)
        
        scene.addChild(submarine)
        
        decreaseOxygen()
    }
    
    private func addControl() {
        switch settings?.control {
        case 0:
            let panGesture = UIPanGestureRecognizer(target: self,
                                                    action: #selector(move(_:)))
            self.view.addGestureRecognizer(panGesture)
        default:
            moveAxel()
        }
    }
    
    
    private func addBubblesEmitter(to scene: SKScene) {
        guard let bubbles = SKEmitterNode(fileNamed: "Waves") else { return }
        bubbles.particlePositionRange.dy = scene.size.height - skyView.frame.height
        bubbles.position = CGPoint(x: scene.frame.width / 2,
                                   y: scene.frame.height / 2 - skyView.frame.height)
        bubbles.zPosition = 0
        scene.addChild(bubbles)
    }
    
    private func addSunEmitter(to scene: SKScene) {
        guard let sun = SKEmitterNode(fileNamed: "Sun") else { return }
        sun.position = CGPoint(x: view.frame.width / 2 + 10,
                               y: view.frame.height - (sun.frame.height / 2) - 20)
        sun.zPosition = 0
        scene.addChild(sun)
    }
    
    private func addSand(to scene: SKScene) {
        sandNode = SKSpriteNode(imageNamed: "sand")
        sandNode.name = "sand"
        sandNode.size = CGSize(width: scene.size.width, height: scene.size.height / 5)
        sandNode.position.x = sandNode.size.width / 2
        sandNode.position.y = 0
        sandNode.colorBlendFactor = 0.2
        scene.addChild(sandNode)
    }
    
    private func addSky(to scene: SKScene) {
        skyNode = SKShapeNode(rectOf: skyView.frame.size)
        skyNode.strokeColor = .clear
        skyNode.position.x = skyView.center.x
        skyNode.position.y = scene.frame.height - skyNode.frame.height / 2
        scene.addChild(skyNode)
    }
    
    private func spawnObstacle(obstacle: Obstacle, completion: (() -> ())? = nil) {
        let textures = getTextures(name: obstacle.name)
        let node = SKSpriteNode(texture: textures.first)
        node.name = obstacle.name
        
        node.xScale = obstacle.scale
        node.yScale = obstacle.scale
        
        var x: CGFloat = 0
        var range: ClosedRange<CGFloat>
        
        switch obstacle.type {
        case .nemoFish, .tigerFish, .yellowFish:
            x = scene.frame.width + node.frame.width
            range = (sandNode.size.height / 2 + node.frame.height / 2)...(skyNode.frame.minY - node.frame.height / 2)
        case .patrickStar:
            x = scene.frame.width + node.frame.width
            range = (node.frame.height / 2)...(skyNode.frame.minY - node.frame.height / 2)
        case .boat:
            x = scene.frame.width + node.frame.width
            range = (skyNode.frame.minY + node.frame.height / 2)...(skyNode.frame.maxY - node.frame.height / 2)
        }
        
        node.position.x = x
        node.position.y = CGFloat.random(in: range)
        scene.addChild(node)
        
        let move = SKAction.moveTo(x: -node.frame.width,
                                   duration: obstacle.speed * axel)
        let animate = SKAction.animate(with: textures,
                                       timePerFrame: obstacle.timePerFrame,
                                       resize: false,
                                       restore: true)
        let repeatAnimate = SKAction.repeatForever(animate)
        let group = SKAction.group([move, repeatAnimate])
        
        node.run(group)
        
        obstaclesTimer = Timer.scheduledTimer(withTimeInterval: obstacle.speed * axel, repeats: false) { _ in
            node.removeAllActions()
            node.removeFromParent()
            completion?()
        }
    }
    
    private func decreaseOxygen() {
        currentAction = .decrease
        let color = SKAction.colorize(with: .red, colorBlendFactor: 1, duration: 10)
        let decrease = SKAction.resize(toWidth: 0, duration: 10)
        let group = SKAction.group([color, decrease])
        oxygenBar.run(group) 
    }
    
    private func restoreOxygen() {
        currentAction = .restore
        oxygenBar.removeAllActions()
        let restore = SKAction.resize(toWidth: submarineNode.frame.width,
                                      duration: 0.1)
        let color = SKAction.colorize(with: .green, colorBlendFactor: 1, duration: 0.1)
        let group = SKAction.group([restore, color])
        oxygenBar.run(group)
    }
    
    private func getTextures(name: String) -> [SKTexture] {
        let textureAtlas = SKTextureAtlas(named: name)
        var textures = [SKTexture]()
        for imageName in textureAtlas.textureNames {
            textures.append(SKTexture(imageNamed: imageName))
        }
        return textures
    }
    
    private func intersection() {
        for node in scene.children {
            if let node = node as? SKSpriteNode, node != submarine, submarineNode.intersects(node) {
                invalidateTimers()
                if node.name == "sand" {
                    createExplosion(position: submarine.position, node: submarineNode, completion: lose)
                } else {
                    createExplosion(position: submarine.position, node: submarineNode)
                    createExplosion(position: node.position, node: node, completion: lose)
                }
                
            }
        }
        
        if submarine.frame.origin.y + submarine.frame.height / 2 > view.frame.height {
            createExplosion(position: submarine.position, node: submarineNode, completion: lose)
        }
        if submarineNode.intersects(skyNode) {
            restoreOxygen()
        } else if currentAction == .restore {
            decreaseOxygen()
        }
    }
    
    private func lose() {
        scene.isPaused = true
        scene.removeAllActions()
        oxygenBar.removeFromParent()
        defaultAxel = nil
        motionManager.stopAccelerometerUpdates()
        saveResult()
        showPauseVC()
    }
    
    private func gameAxeleration() {
        axelTimer = Timer.scheduledTimer(withTimeInterval: 10, repeats: true, block: { _ in
            if self.axel > 0.5 {
                self.axel -= 0.05
            }
        })
    }
    
    private func spawnObstacles() {
        let speed = settings?.speed ?? 1
        
        switch speed {
        case 3:
            spawnObstacle(obstacle: obstacles.randomElement() ?? Obstacle.nemoFish())
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.spawnObstacle(obstacle: self.obstacles.randomElement() ?? Obstacle.nemoFish())
            }
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 2) {
                self.spawnObstacle(obstacle: self.obstacles.randomElement() ?? Obstacle.nemoFish()) {
                    self.spawnObstacles()
                }
            }
        case 2:
            spawnObstacle(obstacle: obstacles.randomElement() ?? Obstacle.nemoFish())
            
            DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                self.spawnObstacle(obstacle: self.obstacles.randomElement() ?? Obstacle.nemoFish()) {
                    self.spawnObstacles()
                }
            }
        default:
            spawnObstacle(obstacle: obstacles.randomElement() ?? Obstacle.nemoFish()) {
                if !self.scene.isPaused {
                    self.spawnObstacles()
                }
                
            }
        }
        
    }
    
    
    private func createExplosion(position: CGPoint, node: SKSpriteNode, completion: (() -> ())? = nil) {
        let explosion = SKSpriteNode(imageNamed: "bang")
        explosion.position = position
        scene.addChild(explosion)
        
        let scaleIn = SKAction.scale(to: 2.5, duration: 0.1)
        let fadeOut = SKAction.fadeOut(withDuration: 0.1)
        let deleteExplosion = SKAction.removeFromParent()
        
        let sequence = SKAction.sequence([scaleIn, fadeOut, deleteExplosion])
        node.removeFromParent()
        node.removeAllActions()
        explosion.run(sequence) {
            completion?()
        }
    }
    
    private func chance(oneIn value: Int) -> Bool {
        let a = Int.random(in: 1...value)
        let b = Int.random(in: 1...value)
        if a == b { return true }
        return false
    }
    
    private func invalidateTimers() {
        intersectionTimer.invalidate()
        scoreTimer.invalidate()
        obstaclesTimer.invalidate()
        axelTimer.invalidate()
    }
    
    private func showPauseVC() {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "PauseViewController") as? PauseViewController else { return }
        controller.needsAnimation = true
        controller.score = self.score
        controller.name = settings?.name
        self.navigationController?.pushViewController(controller, animated: false)
    }
    
    private func resetGame() {
        score = 0
        axel = 1.0
        prepareGameScene()
        addControl()
    }
    
    private func saveResult() {
        let name = settings?.name ?? "User"
        let speed = settings?.speed ?? 1
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd-MM HH:mm"
        let date = dateFormatter.string(from: Date())
        let result = Results(name: name, score: score, speed: speed, date: date)
        
        var results = UserDefaults.standard.value([Results].self, forKey: resultKey) ?? [Results]()
        results.append(result)
        results.sort(by: {$0.score > $1.score})
        if results.count > 10 { results = results.dropLast() }
        
        UserDefaults.standard.set(encodable: results, forKey: resultKey)
    }
    
    private func onAxel() {
        if motionManager.isAccelerometerAvailable {
            motionManager.accelerometerUpdateInterval = 0.1
            motionManager.startAccelerometerUpdates(to: .main) { (data: CMAccelerometerData?, error: Error?) in
                if let acceleration = data?.acceleration {
                    print("x = " + "\(acceleration.x)")
                    print("y = " + "\(acceleration.y)")
                    print("z = " +  "\(acceleration.z)")
                    print()
                }
            }
        }
    }
    
}

