import UIKit

class StartViewController: UIViewController {

    @IBAction func startButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SpriteKitViewController") as? SpriteKitViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func settingsButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "SettingsViewController") as? SettingsViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
    
    @IBAction func scoreButtonPressed(_ sender: UIButton) {
        guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "RecordViewController") as? RecordViewController else { return }
        self.navigationController?.pushViewController(controller, animated: true)
    }
}

