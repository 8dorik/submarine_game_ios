import Foundation


class Game{
    static let shared = Game()
    
    func saveResult(settings: Settings?, score: Int){
        
        guard let name = settings?.name else { return }
        guard let speed = settings?.speed else { return }
        
        let date = Date()
        let formatter = DateFormatter()
        formatter.dateFormat = "MMM d, h:mm a"
        let stringDate = formatter.string(from: date)
        
        var results = [Results]()
        if let array = UserDefaults.standard.value([Results].self, forKey: resultKey){
            results = array
        }
        results.append(Results(name: name , score: score, speed: speed, date: stringDate))
        results.sort(by: {$0.score > $1.score})
        if results.count > 10 {
            results = results.dropLast()
        }
        
        UserDefaults.standard.set(encodable: results, forKey: resultKey)
    }
}
