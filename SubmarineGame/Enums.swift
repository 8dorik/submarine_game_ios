import Foundation

enum SubmarineSkin: String, Codable{
    case gray = "graySubmarine"
    case red = "redSubmarine"
    case purple = "purpleSubmarine"
    case green = "greenSubmarine"
}

enum Obstacle{
    case boat
    case rock
    case fish
}


