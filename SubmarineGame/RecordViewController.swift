import UIKit

class RecordViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    lazy var results = UserDefaults.standard.value([Results].self, forKey: resultKey)
        
    @IBAction func backButtonPressed(_ sender: UIButton) {
        self.navigationController?.popViewController(animated: true)
    }
    
}

extension RecordViewController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 2
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if section == 0 { return 1 }
        
        guard let results = results else { return 0 }
        return results.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "HeaderTableViewCell", for: indexPath) as? HeaderTableViewCell else { return UITableViewCell() }
            
            return cell
        }
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "RecordTableViewCell", for: indexPath) as? RecordTableViewCell else { return UITableViewCell() }
        
        guard let results = results else { return UITableViewCell() }
        let result = results[indexPath.row]
        
        cell.configure(
            name: result.name,
            speed: "\(result.speed)",
            score: "\(result.score)",
            date: result.date
        )
        
        return cell
        
    }
    
    
}
