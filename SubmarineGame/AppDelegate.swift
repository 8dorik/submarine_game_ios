import UIKit
import Firebase
@main
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        FirebaseApp.configure()
        print(Crashlytics.crashlytics().didCrashDuringPreviousExecution())
        return true
    }
    
    func applicationDidFinishLaunching(_ application: UIApplication) {
        FirebaseApp.configure()
    }



}

