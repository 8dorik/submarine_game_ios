import Foundation

class Settings: Codable {
    var submarine: SubmarineSkin = .gray
    var name: String
    var speed = 1
    var control = 0
    
    init(submarine: SubmarineSkin, name: String, speed: Int, control: Int) {
        self.submarine = submarine
        self.name = name
        self.speed = speed
        self.control = control
    }
    
    private enum CodingKeys: String, CodingKey {
        case submarine, name, speed, control
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.name = try container.decode(String.self, forKey: .name)
        self.submarine = try container.decode(SubmarineSkin.self, forKey: .submarine)
        self.speed = try container.decode(Int.self, forKey: .speed)
        self.control = try container.decode(Int.self, forKey: .control)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.name, forKey: .name)
        try container.encode(self.submarine, forKey: .submarine)
        try container.encode(self.speed, forKey: .speed)
        try container.encode(self.control, forKey: .control)
    }
}

