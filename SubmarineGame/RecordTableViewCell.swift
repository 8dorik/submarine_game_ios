import UIKit

class RecordTableViewCell: UITableViewCell {


    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var speedLabel: UILabel!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.isUserInteractionEnabled = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        nameLabel.text = nil
        speedLabel.text = nil
        scoreLabel.text = nil
        dateLabel.text = nil
    }
    
    func configure(name: String, speed: String, score: String, date: String){ 
        nameLabel.text = name
        speedLabel.text = speed
        scoreLabel.text = score
        dateLabel.text = date
    }
    
}
