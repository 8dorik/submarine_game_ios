import Foundation

class Results: Codable{
    var name: String
    var score: Int
    var speed: Int
    var date : String
    
    init(name: String, score: Int, speed: Int, date: String) {
        self.name = name
        self.score = score
        self.speed = speed
        self.date = date
    }
    
    private enum CodingKeys: String, CodingKey {
        case name, score, speed, date
    }

    required public init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)

        self.name = try container.decode(String.self, forKey: .name)
        self.score = try container.decode(Int.self, forKey: .score)
        self.speed = try container.decode(Int.self, forKey: .speed)
        self.date = try container.decode(String.self, forKey: .date)
    }

    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)

        try container.encode(self.name, forKey: .name)
        try container.encode(self.score, forKey: .score)
        try container.encode(self.speed, forKey: .speed)
        try container.encode(self.date, forKey: .date)
    }
    
}
