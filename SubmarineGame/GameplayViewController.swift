import UIKit

class GameplayViewController: UIViewController {
    //MARK: IBOutlets
    @IBOutlet weak var oxygenTrailingConstraint: NSLayoutConstraint!
    @IBOutlet weak var centerSubmarineConstraint: NSLayoutConstraint!
    @IBOutlet weak var gameOverLabel: UILabel!
    @IBOutlet weak var finalScoreLabel: UILabel!
    @IBOutlet var defaultViews: [UIView]!
    @IBOutlet weak var backgroundImageView: UIImageView!
    @IBOutlet weak var submarineView: UIImageView!
    @IBOutlet weak var scoreLabel: UILabel!
    @IBOutlet weak var blurView: UIVisualEffectView!
    
    //MARK: let, var
    let oxygenTime : TimeInterval = 10
    let rockArray = [
        "rock1",
        "rock2",
        "rock3"
    ]
    let fishArray = [
        "fish1",
        "fish2"
    ]
    let supplementArray = [
        "supplement1",
        "supplement2",
        "supplement3"
    ]
    let moveConst: CGFloat = 20
    
    var settings = UserDefaults.standard.value(Settings.self, forKey: settingsKey)
    var boatTime : TimeInterval = 6
    var rockTime : TimeInterval = 9
    var fishTime : TimeInterval = 7
    var score = 0
    var obstaclesArray = [UIView]()
    var timerForScore = Timer()
    var timerForIntersection = Timer()
    var timerForObstacles = Timer()
    var timerForOxygen = Timer()
    var timerForPowersUps = Timer()
    var lose = false
    //MARK: Lifecycle
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        newGameSetup()
    }
    
    //MARK: IBActions
    @IBAction func buttonUpPressed(_ sender: UIButton) {
        centerSubmarineConstraint.constant -= moveConst
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func buttonDownPressed(_ sender: UIButton) {
        centerSubmarineConstraint.constant += moveConst
        UIView.animate(withDuration: 0.3) {
            self.view.layoutIfNeeded()
        }
    }
    
    @IBAction func retryButtonPressed(_ sender: UIButton) {
        blurView.isHidden = true
        viewWillAppear(true)
    }
    
    @IBAction func goToRootButtonPressed(_ sender: UIButton) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    //MARK: Funcs
    private func checkIntersection(){
        for obstacle in obstaclesArray{
            guard let submarineFrame = submarineView.layer.presentation()?.frame else { return }
            guard let obstacleFrame = obstacle.layer.presentation()?.frame else { return }
            
            if submarineFrame.intersects(obstacleFrame){
                lose = true
            }
        }
        
        if submarineView.frame.origin.y <= 0 ||
            submarineView.frame.origin.y + submarineView.frame.height >= self.view.frame.height{
            lose = true
        }
        
        if lose{
            loseGame()
        }
    }
    
    private func createImageView(type: Obstacle) -> UIImageView{
        var height : CGFloat
        var width : CGFloat
        var x : CGFloat
        var y : CGFloat
        
        switch type {
        case .boat:
            height =  CGFloat.random(in: 70...self.view.frame.height / 4)
            width = height
            x = self.view.frame.width
            y = (self.view.frame.height / CGFloat.random(in: 3.5...4)) - height
        case .rock:
            height = CGFloat.random(in: 70...self.view.frame.height / 3.5)
            width = height * 1.5
            x = self.view.frame.width
            y = self.view.frame.height - height
        case .fish:
            height = CGFloat.random(in: 70...self.view.frame.height / 5)
            width = height * 1.5
            x = self.view.frame.width
            y = CGFloat.random(in: (self.view.frame.height / 4)...self.view.frame.height - self.view.frame.height / 3.5 - height)
        }
        
        let view = UIImageView(frame: CGRect(x: x, y: y, width: width, height: height))
        obstaclesArray.append(view)
        return view
    }
    
    private func spawnRock(){
        if rockTime == 0{
            spawnFish()
            return
        }
        
        let rock = createImageView(type: .rock)
        guard let name = rockArray.randomElement() else { return }
        rock.image = UIImage(named: name)
        self.view.addSubview(rock)
        
        UIView.animate(withDuration: rockTime, delay: 0, options: .curveLinear) {
            rock.frame.origin.x = -rock.frame.width
        } completion: { _ in
            self.obstaclesArray = self.obstaclesArray.filter({ $0 != rock })
            rock.removeFromSuperview()
            self.spawnFish()
        }
    }
    
    private func spawnBoat(){
        
        let boat = createImageView(type: .boat)
        boat.image = UIImage(named: "boat1")
        self.view.addSubview(boat)
        
        UIView.animate(withDuration: boatTime, delay: 0, options: .curveLinear) {
            boat.frame.origin.x = -boat.frame.width
        } completion: { _ in
            self.obstaclesArray = self.obstaclesArray.filter({ $0 != boat })
            boat.removeFromSuperview()
            self.spawnRock()
        }
        
    }
    
    private func spawnFish(){
        if fishTime == 0 { return }
        
        let fish = createImageView(type: .fish)
        
        guard let name = fishArray.randomElement() else { return }
        fish.image = UIImage(named: name)
        self.view.addSubview(fish)
        
        UIView.animate(withDuration: fishTime, delay: 0, options: .curveLinear) {
            fish.frame.origin.x = -fish.frame.width
        } completion: { _ in
            self.obstaclesArray = self.obstaclesArray.filter({ $0 != fish })
            fish.removeFromSuperview()
        }
        
    }
    
    private func oxygen(){
        if submarineView.center.y <= self.view.frame.height / 3{
            oxygenTrailingConstraint.constant = 0
            UIView.animate(withDuration: 0.3) {
                self.view.layoutIfNeeded()
            }
            return
        }
        if oxygenTrailingConstraint.constant == -submarineView.frame.width{
            lose = true
            return
        }
        oxygenTrailingConstraint.constant -= submarineView.frame.width / 10
        UIView.animate(withDuration: 1) {
            self.view.layoutIfNeeded()
        }
    }
    
    private func setSpeed(){
        guard let defaults = settings else { return }
        
        switch defaults.speed{
        case 2:
            boatTime = 5
            rockTime = 8
            fishTime = 6
        case 3:
            boatTime = 4
            rockTime = 7
            fishTime = 5
        default:
            boatTime = 6
            rockTime = 9
            fishTime = 7
            
        }
        
        if !defaults.rock{
            rockTime = 0
        }
        if !defaults.fish{
            fishTime = 0
        }
    }
    
    private func timersSetup(){
        
        timerForIntersection = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { _ in
            self.checkIntersection()
        })
        
        timerForObstacles = Timer.scheduledTimer(withTimeInterval: boatTime + fishTime + rockTime, repeats: true, block: { _ in
            self.spawnBoat()
        })
        
        timerForOxygen = Timer.scheduledTimer(withTimeInterval: 1, repeats: true, block: { _ in
            self.oxygen()
        })
        
        timerForScore = Timer.scheduledTimer(withTimeInterval: 0.1, repeats: true, block: { _ in
            self.score += 1
            self.scoreLabel.text = "\(self.score)m"
        })
        
        timerForObstacles.fire()
    }
    
    private func newGameSetup(){
        
        centerSubmarineConstraint.constant = 0
        oxygenTrailingConstraint.constant = 0
        lose = false
        score = 0
        
        guard let defaults = settings else { return }
        submarineView.image = UIImage(named: defaults.submarine.rawValue)
        
        setSpeed()
        timersSetup()
    }
    
    private func loseGame(){
        rockTime = 0
        boatTime = 0
        fishTime = 0
        
        for obstacle in obstaclesArray{
            obstacle.removeFromSuperview()
        }
        obstaclesArray.removeAll()
        
        timerForIntersection.invalidate()
        timerForObstacles.invalidate()
        timerForOxygen.invalidate()
        timerForScore.invalidate()
        
        showGameOverScrceen()
        lose = false
        
        
        Game.shared.saveResult(settings: settings, score: score)
    }
    
    private func showGameOverScrceen(){
        blurView.isHidden = false
        guard let name = settings?.name else { return }
        gameOverLabel.text = "Game over, \(name)"
        finalScoreLabel.text = "Your score is \(score)m"
    }
    
}
