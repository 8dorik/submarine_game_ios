import UIKit
class SettingsViewController: UIViewController {
    
    //MARK: IBOutlets
    @IBOutlet weak var submarineSelectView: UIImageView!
    @IBOutlet weak var nameField: UITextField!
    @IBOutlet weak var gameSpeedLabel: UILabel!
    @IBOutlet weak var gameSpeedStepper: UIStepper!
    @IBOutlet weak var controlTypeSwitch: UISegmentedControl!
    
    //MARK: let, var
    let submarineSkins = [
        SubmarineSkin.gray : UIImage(named: SubmarineSkin.gray.rawValue),
        SubmarineSkin.green : UIImage(named: SubmarineSkin.green.rawValue),
        SubmarineSkin.red: UIImage(named: SubmarineSkin.red.rawValue),
        SubmarineSkin.purple : UIImage(named: SubmarineSkin.purple.rawValue)
    ]
    var settings: Settings?
    var submarineSelected: SubmarineSkin = .gray
    var gameSpeed = 1
    
    //MARK: Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        addTapGesture()
        setDefaults()
        addHideKeyboardGesture()
    }
    
    //MARK: IBActions
    @IBAction func backButtonPressed(_ sender: UIButton) {
        saveSettings()
        popBack()
    }
    
    @IBAction func switchSubmarineSkin(){
        switch submarineSelected{
        case .gray:
            submarineSelected = .red
            guard let image = submarineSkins[submarineSelected] else { return }
            submarineSelectView.image = image
        case .red:
            submarineSelected = .purple
            guard let image = submarineSkins[submarineSelected] else { return }
            submarineSelectView.image = image
        case .purple:
            submarineSelected = .green
            guard let image = submarineSkins[submarineSelected] else { return }
            submarineSelectView.image = image
        case .green:
            submarineSelected = .gray
            guard let image = submarineSkins[submarineSelected] else { return }
            submarineSelectView.image = image
        }
    }
    
    @IBAction func stepperPressed(_ sender: UIStepper){
        gameSpeedLabel.text = Int(gameSpeedStepper.value).description
        gameSpeed = Int(gameSpeedStepper.value)
    }
    
    @objc func hideKeyboard() {
        view.endEditing(true)
    }
    
    private func addHideKeyboardGesture() {
        let tap = UITapGestureRecognizer(target: self, action: #selector(hideKeyboard))
        self.view.addGestureRecognizer(tap)
    }


    //MARK: Funcs
    private func popBack(){
        self.navigationController?.popViewController(animated: true)
    }
    
    private func addTapGesture(){
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(switchSubmarineSkin))
        submarineSelectView.isUserInteractionEnabled = true
        submarineSelectView.addGestureRecognizer(tapGesture)
    }
    
    private func getSettings(){
        guard let defaults = UserDefaults.standard.value(Settings.self, forKey: settingsKey) else { return }
        settings = defaults
    }
    
    private func setDefaults(){
        getSettings()
        
        guard let tempSettings = settings else { return }
        submarineSelected = tempSettings.submarine
        
        guard let image = submarineSkins[submarineSelected] else { return }
        submarineSelectView.image = image
        
        nameField.text = tempSettings.name
        controlTypeSwitch.selectedSegmentIndex = tempSettings.control
        
        gameSpeedStepper.value = Double(tempSettings.speed)
        gameSpeedLabel.text = tempSettings.speed.description
        gameSpeed = tempSettings.speed
    }
    
    private func saveSettings() {
        guard let text = nameField.text else { return }
        settings = Settings(submarine: submarineSelected,
                            name: text,
                            speed: gameSpeed,
                            control: controlTypeSwitch.selectedSegmentIndex)
        UserDefaults.standard.set(encodable: settings, forKey: settingsKey)
    }
}
